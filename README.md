# README #

Add-on to Give WordPress Plugin: https://givewp.com/

## Description ##

This plugin allows user to access a content of a page or a custom post type only after they have made a donation using WordPress Give plugin.

https://wordpress.org/plugins/cip-dtac-for-give/